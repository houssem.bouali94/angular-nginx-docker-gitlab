FROM nginx:latest
COPY dist/angular-docker-nginx-demo /usr/share/nginx/html
COPY nginx/nginx.conf  /etc/nginx/conf.d/default.conf
EXPOSE 80
